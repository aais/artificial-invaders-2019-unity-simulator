
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class KineticSimulator
{
    public float badness; // Something like a relative error
    private float[] drift = {1, 1};

    System.Random rand = new System.Random(); //reuse this if you are generating many

    public void Initialize()
    {
        define_drift();
    }


    private float random_value(float mean, float sd)
    {
        double u1 = 1.0-rand.NextDouble(); //uniform(0,1] random doubles
        double u2 = 1.0-rand.NextDouble();
        double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                         Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
        return (float)(mean + sd * randStdNormal); //random normal(mean,stdDev^2)
    }


    private void define_drift()
    {
        double number = random_value(0, this.badness/2);
        if (number > 0)
        {
            this.drift[0] = (float)(1 - number);
            this.drift[1] = 1F;
        }
        else
        {
            this.drift[1] = (float)(1 + number);
            this.drift[0] = 1F;
        }
    }
    private int counter = 0;

    private void move_wrapper(List<HingeJoint> leftWheels, List<HingeJoint> rightWheels, float leftSpeed, float rightSpeed)
    {
        foreach(HingeJoint lHinge in leftWheels)
        {
            var motor = lHinge.motor;
            motor.force = 60;
            motor.targetVelocity = leftSpeed;
            lHinge.motor = motor;
        }
        foreach(HingeJoint rHinge in rightWheels)
        {
            var motor = rHinge.motor;
            motor.force = 60;
            motor.targetVelocity = rightSpeed;
            rHinge.motor = motor;
        }
        if (counter % 100 == 0)
        {
            Debug.Log(String.Format("Set speed {0} {1}", leftSpeed, rightSpeed));
        }
    }


    public void step_speed(float[] motor_speeds, List<HingeJoint> leftWheels, List<HingeJoint> rightWheels)
    {
        counter += 1;
        if (rand.NextDouble() < Time.fixedDeltaTime / 60) // Expect stuff to happen within a minute
            this.define_drift();
        this.move_wrapper(leftWheels, rightWheels, random_value(motor_speeds[0], motor_speeds[0]*badness)*drift[0], random_value(motor_speeds[1], motor_speeds[1]*badness)*drift[1]);
    }
}

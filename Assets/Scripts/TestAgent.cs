﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAgent : MonoBehaviour
{    InvaderRayPerception invaderRayPerception;

    public List<GameObject> goodBalls = new List<GameObject>();
    public List<GameObject> badBalls = new List<GameObject>();
    float rayDistance = 3.0f;
    float[] rayAngles = { -90f, -45f, -20f, 0f, 20f, 45f, 90f };

    List<float> temp1 = new List<float>();
    List<float> temp2 = new List<float>();
    // Start is called before the first frame update
    void Start()
    {
        invaderRayPerception = GetComponent<InvaderRayPerception>();
    }

    // Update is called once per frame
    void Update()
    {
        temp1.Clear();
        temp2.Clear();
        
        var result1 = invaderRayPerception.Perceive(rayDistance, rayAngles, new List<GameObject>[] {goodBalls});
        temp1.AddRange(result1);
        var result2 = invaderRayPerception.Perceive(rayDistance, rayAngles, new List<GameObject>[] {badBalls});
        temp2.AddRange(result2);

        Debug.Log("========result");
        Debug.Log(string.Join(",", temp1.ToArray()));
        Debug.Log(string.Join(",", temp2.ToArray()));
        Debug.Log("========result");
    }
}

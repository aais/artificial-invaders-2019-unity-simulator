﻿//Put this script on your blue cube.
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;


public class PushAgentBasic : Agent
{
    public bool resetFromNegativeReward;
    public GameObject goodBallPrefab;
    public GameObject badBallPrefab;
    public List<GameObject> levels = new List<GameObject>();
    public List<HingeJoint> leftWheels;
    public List<HingeJoint> rightWheels;

// Realistic gravity  seems to cause some collisions to go undetected
    public float gravity_acc = 9.81f;
    public float ballMass = 0.3f;
    /// <summary>
    /// The ground. The bounds are used to spawn the elements.
    /// </summary>
	private GameObject ground;

    public GameObject area;

    /// <summary>
    /// The area bounds.
    /// </summary>
	[HideInInspector]
    public Bounds areaBounds;

    PushBlockAcademy academy;

    KineticSimulator kinetics;

    /// <summary>
    /// The goal to push the block to.
    /// </summary>
    //public GameObject goal;

    /// <summary>
    /// Detects when the block touches the goal.
    /// </summary>
	[HideInInspector]
    public GoalDetect goalDetect;

    public bool useVectorObs;

    Rigidbody blockRB;  //cached on initialization
    Rigidbody agentRB;  //cached on initialization
    Material groundMaterial; //cached on Awake()
    RayPerception rayPer;

    // float[] rayAngles = { 0f, 45f, 90f, 135f, 180f, 110f, 70f };'
    float[] rayAngles = { -90f, -45f, -20f, 0f, 20f, 45f, 90f };
    string[] detectableObjects = { "homegoal", "opponentgoal", "wall", "opponentagent" };

    /// <summary>
    /// We will be changing the ground material based on success/failue
    /// </summary>
    Renderer groundRenderer;

    private int currentLevel = -1;

    private InvaderRayPerception invaderRayPerception;

    private List<GameObject> goodBalls = new List<GameObject>();
    private List<GameObject> badBalls = new List<GameObject>();

    // private float totalReward = 0.0f;
    private float timeStepRewardMultiplier = 1.0f;

    void Awake()
    {
        academy = FindObjectOfType<PushBlockAcademy>(); //cache the academy
    }

    public override void InitializeAgent()
    {
        base.InitializeAgent();
        rayPer = GetComponent<RayPerception>();
        invaderRayPerception = GetComponent<InvaderRayPerception>();

        // Cache the agent rigidbody
        agentRB = GetComponent<Rigidbody>();

        var resetParams = academy.resetParameters;

        // Initialize kinetics
        kinetics = new KineticSimulator()
        {
            badness = resetParams["robot_badness"],
        };
        kinetics.Initialize();
        SetResetParameters();

        // Use self defined gravity
        ConstantForce gravity = gameObject.AddComponent<ConstantForce>();
        gravity.force = new Vector3(0.0f, -gravity_acc * agentRB.mass, 0.0f);
        agentRB.useGravity = true;
    }

    private void CreateBalls()
    {
        var resetParams = academy.resetParameters;
        int numberGoodBalls = (int) resetParams["number_of_good_balls"];
        int numberBadBalls = (int) resetParams["number_of_bad_balls"];
        int randomizeBallType = (int) resetParams["randomize_ball_type"];
        float ballPositionRandomness = (float) resetParams["ball_position_randomness"];
        //Debug.Log(String.Format("Starting with {0} {1} {2} ", numberGoodBalls, numberBadBalls, ballPositionRandomness));

        DeleteBalls(goodBalls);
        DeleteBalls(badBalls);

        if (randomizeBallType > 0) {
            int selection = UnityEngine.Random.Range(0, 2);
            if (selection == 0) numberBadBalls = 0;
            else numberGoodBalls = 0;
        }

        List<List<Vector3>> positions = GameBallPositions(numberGoodBalls, numberBadBalls);
        InstantiateBallsPositioned(goodBallPrefab, goodBalls, positions[0], ballPositionRandomness);
        InstantiateBallsPositioned(badBallPrefab, badBalls, positions[1], ballPositionRandomness);
    }

    private void DeleteBalls(List<GameObject> ballsList)
    {
        if(ballsList.Count > 0)
        {
            ballsList.ForEach(obj => { Destroy(obj.gameObject); });
            ballsList.Clear();
        }
    }

    private void InstantiateBallsPositioned(GameObject ballPrefab, List<GameObject> ballsList, List<Vector3> positions, float positionRandomness)
    {
        foreach (Vector3 position in positions)
        {
            Vector3 randomizedPosition = GetRandomBallPos(position, positionRandomness);
            GameObject newObj = Instantiate(ballPrefab, randomizedPosition, Quaternion.identity, area.transform);

            ConstantForce gravity = newObj.AddComponent<ConstantForce>();
            gravity.force = new Vector3(0.0f, -gravity_acc * ballMass, 0.0f);
            newObj.GetComponent<Rigidbody>().useGravity = false;

            GoalDetect goalDetect = newObj.GetComponent<GoalDetect>();
            goalDetect.agent = this;
            ballsList.Add(newObj);
        }
    }

    private void InstantiateBalls(GameObject ballPrefab, List<GameObject> ballsList, int amount)
    {
        for (int i = 0; i < amount; ++i)
        {
            GameObject newObj = Instantiate(ballPrefab, GetRandomSpawnPos(2f), Quaternion.identity, area.transform);

            ConstantForce gravity = newObj.AddComponent<ConstantForce>();
            gravity.force = new Vector3(0.0f, -gravity_acc * ballMass, 0.0f);
            newObj.GetComponent<Rigidbody>().useGravity = false;

            GoalDetect goalDetect = newObj.GetComponent<GoalDetect>();
            goalDetect.agent = this;
            ballsList.Add(newObj);
        }
    }

    public override void CollectObservations()
    {
        if (useVectorObs)
        {
            var rayDistance = 40.0f;//56.57f; // SQRT(40**2 + 40**2) = 56.57

            AddVectorObs(rayPer.Perceive(rayDistance, rayAngles, detectableObjects, 1.5f, 0f));
            AddVectorObs(invaderRayPerception.Perceive(rayDistance, rayAngles, new List<GameObject>[] {badBalls, goodBalls}));
        }
    }

    /// <summary>
    /// Get the positions shown in https://drive.google.com/file/d/1nIRF3Cb7aHzOoAHcpqLgCYZtUzii6wFm/view
    /// </summary>
    public List<List<Vector3>> GameBallPositions(int nGood, int nBad)
    {
        List<Vector3> goodPositions = new List<Vector3>()
        {
            new Vector3(0f, 4f, 0f),
            new Vector3(0.4f, 2f, 0f),
            new Vector3(-0.4f, 2f, 0f),
            new Vector3(0f, 2f, 0.4f),
            new Vector3(0f, 2f, -0.4f),
            new Vector3(0.6f, 2f, 0.6f),
            new Vector3(-0.6f, 2f, -0.6f),
        };
        List<Vector3> badPositions = new List<Vector3>()
        {
            new Vector3(0.8f, 2f, -0.2f),
            new Vector3(-0.8f, 2f, 0.2f),
            new Vector3(0.2f, 2f, -0.8f),
            new Vector3(-0.2f, 2f, 0.8f),
        };

        System.Random ran = new System.Random();
        while (0 < goodPositions.Count - nGood)
        {
            int val = ran.Next(0, goodPositions.Count);
            goodPositions.RemoveAt(val);
        }
        while (0 < badPositions.Count - nBad)
            badPositions.RemoveAt(ran.Next(0, badPositions.Count));

        List<List<Vector3>> result = new List<List<Vector3>>()
            {goodPositions, badPositions};
        return result;
    }


    public static float Clamp(float val, float min, float max)
    {
        return (val < min) ? min : (val > max) ? max : val;
    }

    /// <summary>
    /// Use the ground's bounds and a given circle to find a positon for ball
    /// </summary>
    public Vector3 GetRandomBallPos(Vector3 center, float radius)
    {
        //Debug.Log(String.Format("Trying to place ball to {0} {1} {2}", center[0], center[1], center[2]));
        //Debug.Log(String.Format("The x extent is {0} and z {1}. Radius {2}", areaBounds.extents.x * academy.spawnAreaMarginMultiplier,
        //        areaBounds.extents.z * academy.spawnAreaMarginMultiplier, radius));

        var resetParams = academy.resetParameters;

        int maxTries = 10;
        int currentTry = 0;
        bool foundNewSpawnLocation = false;
        Vector3 randomSpawnPos = Vector3.zero;

        float r = UnityEngine.Random.Range(0, radius);

        while (foundNewSpawnLocation == false && currentTry < maxTries)
        {
            Vector2 dir = UnityEngine.Random.insideUnitCircle;

            float randomPosX =  areaBounds.extents.x * academy.spawnAreaMarginMultiplier
                            * center[0] + dir[0] * r;
            float randomPosZ = areaBounds.extents.z * academy.spawnAreaMarginMultiplier
                            * center[2] + dir[1] * r;

            randomPosX = Clamp(randomPosX,
                            -areaBounds.extents.x * academy.spawnAreaMarginMultiplier,
                            areaBounds.extents.x * academy.spawnAreaMarginMultiplier
            );
            randomPosZ = Clamp(randomPosZ,
                    -areaBounds.extents.z * academy.spawnAreaMarginMultiplier,
                    areaBounds.extents.z * academy.spawnAreaMarginMultiplier
            );

            randomSpawnPos = ground.transform.position + new Vector3(randomPosX, center[1], randomPosZ);

            //Debug.Log(String.Format("In other coordinates ball to {0} {1} {2}", randomSpawnPos[0], randomSpawnPos[1], randomSpawnPos[2]));
            if (Physics.CheckBox(randomSpawnPos, new Vector3(resetParams["ball_scale"] + 0.2f, 0.1f, resetParams["ball_scale"] + 0.2f)) == false)
            {
                foundNewSpawnLocation = true;
            }
            currentTry++;
        }
        if (foundNewSpawnLocation) return randomSpawnPos;
        Debug.Log("Couldnt find optimal ball place");
        return GetRandomSpawnPos(center[1]);
    }


    /// <summary>
    /// Use the ground's bounds to pick a random spawn position.
    /// </summary>
    public Vector3 GetRandomSpawnPos(float height)
    {
        var resetParams = academy.resetParameters;

        int maxTries = 500;
        int currentTry = 0;
        bool foundNewSpawnLocation = false;
        Vector3 randomSpawnPos = Vector3.zero;
        while (foundNewSpawnLocation == false && currentTry < maxTries)
        {
            float randomPosX = UnityEngine.Random.Range(-areaBounds.extents.x * academy.spawnAreaMarginMultiplier,
                                areaBounds.extents.x * academy.spawnAreaMarginMultiplier);

            float randomPosZ = UnityEngine.Random.Range(-areaBounds.extents.z * academy.spawnAreaMarginMultiplier,
                                            areaBounds.extents.z * academy.spawnAreaMarginMultiplier);
            randomSpawnPos = ground.transform.position + new Vector3(randomPosX, height, randomPosZ);
            if (Physics.CheckBox(randomSpawnPos, new Vector3(resetParams["ball_scale"] + 0.3f, 0.01f, resetParams["ball_scale"] + 0.3f)) == false)
            {
                foundNewSpawnLocation = true;
            }
            currentTry++;
        }
        if (foundNewSpawnLocation) return randomSpawnPos;

        Debug.Log(String.Format("Could not find a place for the object with height {0}.", height));
        throw new Exception();
    }

    /// <summary>
    /// Called when the agent moves the block into the goal.
    /// </summary>
    public void IScoredAGoal(bool opponentGoal, bool goodBall, GameObject ball)
    {
        DestroyBall(goodBall, ball);

        float reward = CalculateReward(opponentGoal, goodBall);
        AddReward(reward);

        Material mat =  null;
        if (reward > 0)
        {
            mat = academy.rightGoalScoredMaterial;
        }
        else
        {
            mat = academy.wrongGoalScoredMaterial;
        }

        // Swap ground material for a bit to indicate we scored.
        StartCoroutine(GoalScoredSwapGroundMaterial(mat, 0.5f));

        IsGameFinished(reward);
    }

    private void DestroyBall(bool goodBall, GameObject ball)
    {
        if(goodBall)
        {
            goodBalls.Remove(ball);
        }
        else
        {
            badBalls.Remove(ball);
        }
        Destroy(ball.gameObject);
    }

    private float CalculateReward(bool opponentGoal, bool goodBall)
    {
        var resetParams = academy.resetParameters;

        float goodBallWrongGoalReward = resetParams["good_ball_wrong_goal_reward"];
        float goodBallRightGoalReward = resetParams["good_ball_right_goal_reward"];
        float badBallWrongGoalReward = resetParams["bad_ball_wrong_goal_reward"];
        float badBallRightGoalReward = resetParams["bad_ball_right_goal_reward"];

        float reward = 0.0f;
        if (goodBall && opponentGoal) reward = goodBallWrongGoalReward;
        else if (goodBall && !opponentGoal) reward = goodBallRightGoalReward;
        else if (!goodBall && !opponentGoal) reward = badBallWrongGoalReward;
        else if (!goodBall && opponentGoal) reward = badBallRightGoalReward;
        return reward;
    }

    private void IsGameFinished(float reward)
    {
        if (resetFromNegativeReward && reward < 0.0f)
        {
            Done();
        }
        else if (goodBalls.Count == 0 && badBalls.Count == 0)
        {
            Done();
        }
    }

    /// <summary>
    /// Swap ground material, wait time seconds, then swap back to the regular material.
    /// </summary>
    IEnumerator GoalScoredSwapGroundMaterial(Material mat, float time)
    {
        groundRenderer.material = mat;
        yield return new WaitForSeconds(time); // Wait for 2 sec
        groundRenderer.material = groundMaterial;
    }

    /// <summary>
    /// Moves the agent according to the selected action.
    /// </summary>
    int gocounter = -100;
	public void MoveAgent(float[] act)
    {

        Vector3 dirToGo = Vector3.zero;
        Vector3 rotateDir = Vector3.zero;

        int action = Mathf.FloorToInt(act[0]);


        //if (gocounter < 0)
        //    action = 0;
        //else if (gocounter < 100)
        //{
        //    Debug.Log("Turning");
        //    action = 3;
        //}
        //else if (gocounter < 300)
        //{
        //    Debug.Log("Turning left");
        //    action = 4;
        //}
        //else if (gocounter < 450)
        //{
        //    Debug.Log("forward");
        //    action = 1;
        //}
        //else if (gocounter < 500)
        //{
        //    Debug.Log("Back");
        //    action = 2;
        //}
        //else if (gocounter < 600)
        //{
        //    Debug.Log("Curve");
        //    action = 5;
        //}
        //else
        //    action = Mathf.FloorToInt(UnityEngine.Random.Range(1, 6.5f));
        //Gocounter += 1;

        // Goalies and Strikers have slightly different action spaces.
        float[] speeds = {0, 0};
        switch (action)
        {
            case 1:
                speeds[0] = 1f;
                speeds[1] = 1f;
                break;
            case 2:
                speeds[0] = -1f;
                speeds[1] = -1f;
                break;
            case 3:
                speeds[0] =  1f;
                speeds[1] = -1f;
                break;
            case 4:
                speeds[0] = -1f;
                speeds[1] =  1f;
                break;
            case 5:
                speeds[0] = 0.5f;
                speeds[1] =  1f;
                break;
            case 6:
                speeds[0] =  1f;
                speeds[1] = 0.5f;
                break;
        }
        // w = phi / t
        // s = phi * r
        // w = s/t / r
        speeds[0] *= academy.agentRunSpeed * 180 / (float)Math.PI / 0.8f;
        speeds[1] *= academy.agentRunSpeed * 180 / (float)Math.PI / 0.8f;
        kinetics.step_speed(speeds, leftWheels, rightWheels);

    }

    /// <summary>
    /// Called every step of the engine. Here the agent takes an action.
    /// </summary>
	public override void AgentAction(float[] vectorAction, string textAction)
    {
        // Move the agent using the action.
        MoveAgent(vectorAction);

        // Penalty given each step to encourage agent to finish task quickly.
        AddReward((-1f / agentParameters.maxStep) * timeStepRewardMultiplier);
    }


    /// <summary>
    /// In the editor, if "Reset On Done" is checked then AgentReset() will be
    /// called automatically anytime we mark done = true in an agent script.
    /// </summary>
	public override void AgentReset()
    {
        // totalReward = 0.0f;
        int rotation = UnityEngine.Random.Range(0, 4);
        float rotationAngle = rotation * 90f;
        area.transform.Rotate(new Vector3(0f, rotationAngle, 0f));

        // ResetBalls();
        CreateBalls();
        agentRB.velocity = Vector3.zero;
        agentRB.angularVelocity = Vector3.zero;
        transform.position = GetRandomSpawnPos(0.6f);
        agentRB.velocity = Vector3.zero;
        agentRB.angularVelocity = Vector3.zero;

        SetResetParameters();
    }

    public void SetGroundMaterialFriction()
    {
        var resetParams = academy.resetParameters;

        var groundCollider = ground.GetComponent<Collider>() as Collider;

        groundCollider.material.dynamicFriction = resetParams["dynamic_friction"];
        groundCollider.material.staticFriction = resetParams["static_friction"];
    }

    public void SetBallsProperties()
    {
        var resetParams = academy.resetParameters;
        foreach(List<GameObject> ballList in new List<GameObject>[] {goodBalls, badBalls})
        {
            foreach(GameObject ball in ballList)
            {
                // ball.transform.localScale = new Vector3(resetParams["ball_scale"], resetParams["ball_scale"], resetParams["ball_scale"]);
                ball.transform.localScale = new Vector3(resetParams["ball_scale"], resetParams["ball_scale"], resetParams["ball_scale"]);

                ball.GetComponent<Rigidbody>().drag = resetParams["ball_drag"];
            }
        }
    }

    public void SetResetParameters()
    {
        SetLevel();
        SetGroundMaterialFriction();
        SetBallsProperties();
    }

    private void SetLevel()
    {
        var resetParams = academy.resetParameters;
        var newLevel = (int) resetParams["level_number"];

        if (currentLevel == newLevel) return;
        currentLevel = newLevel;

        for (int i = 0; i < levels.Count; ++i)
        {
            if (i != currentLevel) levels[i].SetActive(false);
            else levels[i].SetActive(true);
        }

        ground = levels[currentLevel].transform.Find("Ground").gameObject; //GameObject.FindWithTag("ground");

        // // Get the ground's bounds
        areaBounds = ground.GetComponent<Collider>().bounds;
        // // Get the ground renderer so we can change the material when a goal is scored
        groundRenderer = ground.GetComponent<Renderer>();
        // // Starting material
        groundMaterial = groundRenderer.material;
        agentParameters.maxStep = (int) resetParams["agent_max_steps"];
        timeStepRewardMultiplier = resetParams["time_step_reward_multiplier"];

        CreateBalls();
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class InvaderRayPerception : MonoBehaviour
{
    private List<float> perceptionBuffer = new List<float>();
    private float[] segmentSubList;

    public List<float> Perceive(
        float rayDistance,
        float[] rayAngles,
        List<GameObject>[] objectListArray)
    {   
        Vector2 origo = new Vector2(transform.position.x, transform.position.z);
        Vector2[] rayVectors = CreateRayVectors(rayAngles, rayDistance);
        Vector2[][] segments = CreateSegments(rayVectors, origo);
        return CheckBallObservation(segments, origo, rayDistance, objectListArray);
    }

    private Vector2[] CreateRayVectors(float[] rayAngles, float rayDistance)
    {
        Vector3 forwardVec = transform.forward;
        Vector2[] rayVectors = new Vector2[rayAngles.Length];
        for (var i = 0; i < rayAngles.Length; i++)
        {
            Vector3 dirVec = new Vector3(forwardVec.x, forwardVec.y, forwardVec.z);
            Vector3 newVec =  Quaternion.Euler(0, rayAngles[i], 0) * dirVec * rayDistance;

            rayVectors[i] = new Vector2(newVec.x, newVec.z);
            //Debug.DrawRay(transform.position, newVec, Color.yellow, 0.1f, true);
        }

        return rayVectors;
    }

    private Vector2[][] CreateSegments(Vector2[] rayVectors, Vector2 origo)
    {
        Vector2[][] segments = new Vector2[rayVectors.Length - 1][];
        for (var i = 0; i < rayVectors.Length - 1; i++)
        {
            Vector2[] segmentArray = new Vector2[3];
            segmentArray[0] = origo;
            segmentArray[1] = origo + rayVectors[i];
            segmentArray[2] = origo + rayVectors[i+1];
            segments[i] = segmentArray;
        }
        if (Application.isEditor)
        {
            DrawSegments(segments);
        }
        return segments;
    }

    // The data array structure for each segment is like below.
    // "ballSubList"-array is all segments' results in one array
    // Array structure for one segment:
    // | good ball | bad ball | no ball | distance to ball |
    // | 1 or 0    | 1 or 0   | 1 or 0  | 0 - 1            |
    private List<float> CheckBallObservation(Vector2[][] segments, Vector2 origo, float rayDistance, List<GameObject>[] objectListArray)
    {
        if (segmentSubList == null || segmentSubList.Length != objectListArray.Length + 2)
            segmentSubList = new float[objectListArray.Length + 2];

        perceptionBuffer.Clear();
        perceptionBuffer.Capacity = segmentSubList.Length * segments.Length;

        for (var i = 0; i < segments.Length; i++)
        {
            Array.Clear(segmentSubList, 0, segmentSubList.Length);
            // Set the "no objects found inside segment" bit (The 2nd last bit) to 1
            segmentSubList[segmentSubList.Length - 2] = 1.0f; 
            int activeGroupIndex = 0;
            foreach(List<GameObject> ballList in objectListArray)
            {
                foreach (GameObject ball in ballList)
                {
                    Vector2 ballCenter = new Vector2(ball.transform.position.x, ball.transform.position.z);
                    if (IsPointInPolygon4(segments[i], ballCenter))
                    {
                        float newDist = Vector2.Distance(origo, ballCenter) / rayDistance;
                        // Debug.Log("========Ball inside segment: " + i + ", Ball type: " + activeBallGroupIndex + ", Distance: " + newDist + ", Old distance: " + segmentSubList[segmentSubList.Length - 1]);
                        if (newDist < segmentSubList[segmentSubList.Length - 1] || segmentSubList[segmentSubList.Length - 2] > 0.0f)
                        {
                            // Set object indexes to 0 and after that set the found object index to 1
                            for(var j = 0; j < objectListArray.Length; j++)
                            {
                                segmentSubList[j] = 0.0f;
                            }
                            segmentSubList[activeGroupIndex] = 1.0f;
                            segmentSubList[segmentSubList.Length - 2] = 0.0f;
                            segmentSubList[segmentSubList.Length - 1] = newDist;
                        }
                    }
                }
                activeGroupIndex++;
            }
            // Debug.Log(string.Join("#", segmentSubList));
            Utilities.AddRangeNoAlloc(perceptionBuffer, segmentSubList);
        }
        
        // Debug.Log("========perceptionBuffer");
        // Debug.Log(string.Join(",", perceptionBuffer.ToArray()));
        // Debug.Log("========perceptionBuffer");
        return perceptionBuffer;
    }

     /// <summary>
    /// Determines if the given point is inside the polygon
    /// </summary>
    /// <param name="polygon">the vertices of polygon</param>
    /// <param name="testPoint">the given point</param>
    /// <returns>true if the point is inside the polygon; otherwise, false</returns>
    public static bool IsPointInPolygon4(Vector2[] polygon, Vector2 testPoint)
    {
        bool result = false;
        int j = polygon.Length - 1;
        for (int i = 0; i < polygon.Length; i++)
        {
            if (polygon[i].y < testPoint.y && polygon[j].y >= testPoint.y || polygon[j].y < testPoint.y && polygon[i].y >= testPoint.y)
            {
                if (polygon[i].x + (testPoint.y - polygon[i].y) / (polygon[j].y - polygon[i].y) * (polygon[j].x - polygon[i].x) < testPoint.x)
                {
                    result = !result;
                }
            }
            j = i;
        }
        return result;
    }

    private void DrawSegments(Vector2[][] segments)
    {
        foreach (Vector2[] segment in segments)
        {
            Vector3 start = new Vector3(segment[0].x, 0.0f, segment[0].y);
            Vector3 endOne = new Vector3(segment[1].x, 0.0f, segment[1].y);
            Vector3 endTwo = new Vector3(segment[2].x, 0.0f, segment[2].y);
            Debug.DrawRay(start, endOne, Color.green, 0.1f, true);
            Debug.DrawRay(start, endTwo, Color.green, 0.1f, true);
            Debug.DrawLine(endTwo, endOne, Color.green, 0.1f, true);
        }
    }
}